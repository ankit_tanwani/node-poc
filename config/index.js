var _ = require('lodash'),
    defaults = {};

_.set(defaults, "Constants", {
    SESSION_COOKIE: "brain4ce_n",
    BATCH: 1,
    USER: 2,
    CUSTOMER: 3,
    USER_LEAD: 4,
    WEBINAR: 5,
    ALL_USER: 6,
    USER_LIST: 7,
    CHANNEL_DESKTOP: 1,
    CHANNEL_ANDROID: 2,
    CHANNEL_BLOCKER: 3,
    CHANNEL_IOS: 4,
    CHANNEL_EMAIL: 5,
    PROCESSED_STATUS: 3,
    CHANNEL_ID: 1,
    DEFAULT_ACTION: null
});

function __getEnvConfig() {
    switch (process.env.NODE_ENV) {
        case 'production':
            return require('./lib/prod');
        case 'test':
            return require('./lib/test');
        default:
            return require('./lib/dev');
    }
}

config = {};
exports = module.exports = _.defaultsDeep(config, __getEnvConfig(), defaults);