var _ = require('lodash');
var config = {};

_.set(config, "db.database", 'edureka-test');
_.set(config, "db.port", 3306);

_.set(config, "db.master", {
    host: '52.26.225.168',
    user: 'test',
    password: 'VhaciRty9',
    // database: 'edureka-test'
});

_.set(config, "db.slave", {
    host: '52.26.225.168',
    user: 'test',
    password: 'VhaciRty9',
    // database: 'edureka-test'
});

_.set(config, "pool.master", {
    max: 1,
    min: 0,
    acquire: 30000,
    idle: 10000
});

_.set(config, "pool.slave", {
    max: 1,
    min: 0,
    acquire: 30000,
    idle: 10000
});

_.set(config, "cache.redis", {
    'host': '127.0.0.1',
    'port': 6379,
    // 'auth': 'default',
    // 'persistent': false,
    'index': 0,
});

_.set(config, "urls", {
    'base_url': 'https://test.edureka.in/',
    'image_base_url': 'https://test.edureka.in/img/',
    'learning_url': 'https://learning.edureka.in/',
    'forum_url': 'https://forum.edureka.in/',
    'support_url': 'https://help.edureka.in/',
});

_.set(config, 'auth', {
    use_cookie: false,
    auth_verify_url: 'http://auth.dev-edureka.co/api/user/verify'
});

exports = module.exports = config;
