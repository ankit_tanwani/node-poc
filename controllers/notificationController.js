var config = require('../config');

function getNotifications(req, res, next) {
	var notificationService = require('../services/notificationService');
	var channel_id, action_id;
	if (req.query_params.channel_id) {
		channel_id = query_params.channel_id;
	} else {
		channel_id = config.Constants.CHANNEL_ID;
	}
	if (req.query_params.action_id) {
		action_id = query_params.action_id;
	} else {
		action_id = config.Constants.ACTION_ID;
	}
	var notificationsData = notificationService.getNotificationCountAndData(req.session_data, req.pagination, channel_id, action_id);
	var notificationsCount = notificationService.getNewNotificationCount(req.session_data, channel_id);
	Promise.all([notificationsData, notificationsCount]).then((data) => {
		res.send({
			status_code: 200, message: "Notifications",
			data: {
				items: data[0].items,
				newcount: data[1]
			},
			pagination: data[0].pagination,
		});
	}
	).catch((error) => {
		next(error);
	});
}

module.exports = {
	getNotifications: getNotifications
}
