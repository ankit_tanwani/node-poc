const Sequelize = require('sequelize');
const sequelize = require('../seq');
module.exports = sequelize.define('channel', {
  name: Sequelize.STRING(50),
  description: Sequelize.STRING(50)
}, {
    tableName: 'ne_channels',
    timestamps: false
  });