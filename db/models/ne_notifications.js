const Sequelize = require('sequelize');
const sequelize = require('../seq');
const NeTrigger = require('./ne_triggers');
const NeNotification = sequelize.define('ne_notification', {
  id: {
    type: Sequelize.INTEGER(11),
    allowNull: false,
    primaryKey: true,
    autoIncrement: true
  },
  group_type_id: {
    type: Sequelize.INTEGER(11),
    allowNull: false,
    defaultValue: '0',
    references: {
      model: 'ne_group_types',
      key: 'id'
    }
  },
  group_value: {
    type: Sequelize.STRING(255),
    allowNull: true
  },
  description: {
    type: Sequelize.STRING(500),
    allowNull: true
  },
  url: {
    type: Sequelize.STRING(250),
    allowNull: true
  },
  api_parameters: {
    type: Sequelize.TEXT,
    allowNull: true
  },
  zoho_api_parameter: {
    type: Sequelize.STRING(1024),
    allowNull: true
  },
  repeat_times: {
    type: Sequelize.INTEGER(2),
    allowNull: true
  },
  schedule_time: {
    type: Sequelize.DATE,
    allowNull: true
  },
  expiry_date: {
    type: Sequelize.DATE,
    allowNull: true
  },
  is_schedule: {
    type: Sequelize.INTEGER(1),
    allowNull: true,
    defaultValue: '0'
  },
  processed_status: {
    type: Sequelize.INTEGER(4),
    allowNull: true,
    defaultValue: '0'
  },
  zoho_id: {
    type: Sequelize.STRING(45),
    allowNull: true
  },
  created: {
    type: Sequelize.DATE,
    allowNull: true
  },
  created_by: {
    type: Sequelize.INTEGER(11),
    allowNull: true
  },
  modified: {
    type: Sequelize.DATE,
    allowNull: true
  },
  scheduler_msg: {
    type: Sequelize.STRING(45),
    allowNull: true
  },
  trigger_id: {
    type: Sequelize.INTEGER(11),
    allowNull: true
  },
  uuid: {
    type: Sequelize.STRING(45),
    allowNull: true
  }
}, {
    tableName: 'ne_notifications',
    timestamps: false
  });

NeNotification.belongsTo(NeTrigger, { foreignKey: 'trigger_id' });
module.exports = NeNotification;
