const Sequelize = require('sequelize');
const sequelize = require('../seq');
const NeTriggerChannels = sequelize.define('ne_trigger_channel', {
  id: {
    type: Sequelize.INTEGER(11),
    allowNull: false,
    primaryKey: true,
    autoIncrement: true
  },
  trigger_id: {
    type: Sequelize.INTEGER(11),
    allowNull: false
  },
  channel_id: {
    type: Sequelize.INTEGER(11),
    allowNull: false
  },
  template: {
    type: Sequelize.TEXT,
    allowNull: true
  },
  outputtype: {
    type: Sequelize.STRING(45),
    allowNull: true
  },
  target: {
    type: Sequelize.STRING(255),
    allowNull: true
  },
  url_target: {
    type: Sequelize.STRING(450),
    allowNull: true
  },
  mobile_target: {
    type: Sequelize.STRING(45),
    allowNull: true
  },
  modified: {
    type: Sequelize.DATE,
    allowNull: true
  },
  created: {
    type: Sequelize.DATE,
    allowNull: true
  },
  status: {
    type: Sequelize.INTEGER(4),
    allowNull: true,
    defaultValue: '1'
  }
}, {
    tableName: 'ne_trigger_channels',
    timestamps: false
  });


// const NeTriggers = require('./ne_triggers');
// NeTriggerChannels.belongsTo(NeTriggers, { foreignKey: 'trigger_id' });

module.exports = NeTriggerChannels;
