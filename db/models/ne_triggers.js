const Sequelize = require('sequelize');
const sequelize = require('../seq');

const NeTriggers = sequelize.define('ne_trigger', {
  id: {
    type: Sequelize.INTEGER(11),
    allowNull: false,
    primaryKey: true,
    autoIncrement: true
  },
  name: {
    type: Sequelize.STRING(150),
    allowNull: false
  },
  unique_name: {
    type: Sequelize.STRING(100),
    allowNull: true,
    unique: true
  },
  function_name: {
    type: Sequelize.STRING(45),
    allowNull: false
  },
  expire_after: {
    type: Sequelize.STRING(150),
    allowNull: true,
    defaultValue: '+1 month'
  },
  default_template: {
    type: Sequelize.STRING(2000),
    allowNull: false
  },
  created: {
    type: Sequelize.DATE,
    allowNull: true
  },
  modified: {
    type: Sequelize.DATE,
    allowNull: true
  },
  status: {
    type: Sequelize.INTEGER(4),
    allowNull: true,
    defaultValue: '0'
  }
}, {
    tableName: 'ne_triggers',
    timestamps: false
  });

const NeTriggerChannels = require('./ne_trigger_channels');
NeTriggers.hasMany(NeTriggerChannels, { foreignKey: 'trigger_id' });

module.exports = NeTriggers;

