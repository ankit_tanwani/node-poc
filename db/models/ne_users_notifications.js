const Sequelize = require('sequelize');
const sequelize = require('../seq');
const NeNotifications = require('./ne_notifications');
const NeUserNotifications = sequelize.define('ne_users_notification', {
  id: {
    type: Sequelize.INTEGER(11),
    allowNull: false,
    primaryKey: true,
    autoIncrement: true
  },
  user_id: {
    type: Sequelize.INTEGER(11),
    allowNull: true,
    references: {
      model: 'users',
      key: 'id'
    }
  },
  notification_id: {
    type: Sequelize.INTEGER(11),
    allowNull: true,
    references: {
      model: 'ne_notifications',
      key: 'id'
    }
  },
  message: {
    type: Sequelize.STRING(1000),
    allowNull: true
  },
  url: {
    type: Sequelize.STRING(250),
    allowNull: true
  },
  api_parameters: {
    type: Sequelize.STRING(512),
    allowNull: true
  },
  action_id: {
    type: Sequelize.INTEGER(11),
    allowNull: true
  },
  action_time: {
    type: Sequelize.DATE,
    allowNull: true
  },
  channel_id: {
    type: Sequelize.INTEGER(11),
    allowNull: true
  }
}, {
    tableName: 'ne_users_notifications',
    timestamps: false
  });

NeUserNotifications.belongsTo(NeNotifications, { foreignKey: 'notification_id' });
module.exports = NeUserNotifications;

