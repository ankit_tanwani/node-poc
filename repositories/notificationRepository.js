var _ = require('lodash');
var config = require('../config');
const Sequelize = require('sequelize');
const NeUserNotification = require('../db/models/ne_users_notifications');
const NeNotification = require('../db/models/ne_notifications');
const NeTrigger = require('../db/models/ne_triggers');
const NeTriggerChannels = require('../db/models/ne_trigger_channels');

// const redis = require('../core/cache');

function getNotificationCountAndData(userData, pagination, channel_id, action_id) {
    // redis.set('test_node','123');
    return new Promise(function (resolve, reject) {
        var query = {
            offset: (pagination.page - 1) * pagination.limit,
            limit: pagination.limit,
            subQuery: false,
            order: [
                ['action_id', 'ASC'],
                [NeNotification, 'schedule_time', 'DESC']
            ],
        };
        _.defaultsDeep(query, coreNotificationQuery(userData, channel_id, action_id));
        NeUserNotification.findAndCountAll(query)
            .then(result => {
                resolve({
                    total: result.count,
                    items: result.rows
                });
            }).catch(err => {
                reject(err);
            });
    });
}

function getNewNotificationCount(userData, channel_id) {
    return new Promise(function (resolve, reject) {
        var query = {
            where: {
                action_id: config.Constants.DEFAULT_ACTION
            },
            subQuery: false,
        };
        _.defaultsDeep(query, coreNotificationQuery(userData, channel_id));
        NeUserNotification.count(query)
            .then(items => {
                resolve(items);
            }).catch(err => {
                reject(err);
            });
    });
}

function coreNotificationQuery(userData, channel_id, action_id) {
    var query = {
        where: {
            user_id: userData.id
        },
        include: [{
            model: NeNotification,
            required: true,
            // order: [['schedule_time', 'DESC']],
            where: {
                is_schedule: 1,
                processed_status: config.Constants.PROCESSED_STATUS,
                schedule_time: {
                    $lte: Sequelize.fn('NOW')
                },
                $or: [
                    {
                        expiry_date: {
                            $gte: Sequelize.fn('NOW')
                        }
                    },
                    {
                        expiry_date: null
                    },
                    {
                        expiry_date: Sequelize.literal("`ne_notification`.`expiry_date` = '0000-00-00 00:00:00'")
                    }
                ]
            },
            include: [
                {
                    model: NeTrigger,
                    required: true,
                    where: {
                        status: 1
                    },
                    include: [
                        {
                            model: NeTriggerChannels,
                            required: true,
                            where: {
                                status: 1,
                                channel_id: channel_id
                            }
                        }
                    ]
                }
            ]
        }],
        // order: [['action_id', 'ASC']],
    };
    if (action_id) {
        query.where.action_id = action_id;
    }
    return query;
}

module.exports = {
    getNotificationCountAndData: getNotificationCountAndData,
    getNewNotificationCount: getNewNotificationCount,
}