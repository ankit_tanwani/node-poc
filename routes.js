exports = module.exports = [
    ['/notifications/list',
        'notificationController#getNotifications',
        'authMiddleware#authenticated',
        'queryParamsMiddleware#getPaginator',
        'GET'],
];
