const _ = require('lodash');
var config = require('../config');
var common_utils = require('../utilities/commonUtility');

function getNotificationCountAndData(user_data, pagination, channel_id, action_id) {
    var notificationsRepo = require('../repositories/notificationRepository');
    var notificationsDataAndCount = notificationsRepo.getNotificationCountAndData(user_data, pagination, channel_id, action_id);
    return new Promise(function (resolve, reject) {
        notificationsDataAndCount.then((notificationData) => {
            var paginatorUtility = require('../utilities/paginatorUtility');
            var updatedPagination = paginatorUtility.updatePagination(pagination.page, pagination.limit, notificationData.total);

            var items = notificationData.items;
            final_notifications = [];
            if (items.length) {
                notification_url_domain = ['https://test.edureka.in/', 'https://www.edureka.co/'];
                items.forEach(notification => {
                    trigger_id = notification.ne_notification.trigger_id;

                    api_params = JSON.parse(notification.ne_notification.api_parameters);
                    if (api_params.email_params) {
                        delete api_params.email_params;
                    }
                    notification_url = _.replace(notification.url, 'http://', 'https://');
                    if (trigger_id == 25 || trigger_id == 26) {
                        ///referral based notifications
                        notification_url = config.urls.base_url + 'refer';
                    }

                    if (notification_url.indexOf('/tickets') != -1) {
                        //tickets notifications 
                        notification_url = _.replace(notification_url, notification_url_domain, config.urls.support_url);
                    } else if (notification_url.indexOf('/communities/') != -1) {
                        //communities forum notifications 
                        notification_url = _.replace(notification_url, notification_url_domain, config.urls.forum_url);
                    }

                    var timestamp = new Date(notification.ne_notification.schedule_time).getTime() / 1000;

                    parsed_notification = {
                        'id': notification.ne_notification.id,
                        'group_type_id': notification.ne_notification.group_type_id,
                        'name': notification.ne_notification.ne_trigger.name,
                        'unique_name': notification.ne_notification.ne_trigger.unique_name,
                        'outputtype': notification.ne_notification.ne_trigger.ne_trigger_channels.outputtype,
                        'target': notification.ne_notification.ne_trigger.ne_trigger_channels.target,
                        'message': notification.message ? notification.message : notification.ne_notification.description,
                        'notificationurl': notification.ne_notification.url,
                        'description': notification.ne_notification.description,
                        'schedule_time': notification.ne_notification.schedule_time,
                        'timestamp': timestamp,
                        'timestamp_message': common_utils.timeStampWebinar(timestamp),
                        'usernotificationurl': notification_url,
                        'api_parameters': api_params,
                        'action_id': (notification.action_id && notification.action_id == 3) ? true : false,
                        'trigger_id': trigger_id
                    }

                    if (notification.ne_notification.group_type_id && notification.ne_notification.group_type_id == config.Constants.Webinar) {
                        parsed_notification.notificationurl = notification.url;
                    }
                    if (channel_id != config.Constants.ChannelDesktop && notification.api_parameters) {
                        parsed_notification.mobileparams = JSON.parse(notification.api_parameters);
                    }
                    final_notifications.push(parsed_notification);
                });
            }
            resolve({
                pagination: updatedPagination,
                items: final_notifications
            });
        }).catch((err) => {
            reject(err);
        });
    });
}

function getNewNotificationCount(user_data, channel_id) {
    var notificationsRepo = require('../repositories/notificationRepository');
    var newNotificationsCount = notificationsRepo.getNewNotificationCount(user_data, channel_id);
    return new Promise(function (resolve, reject) {
        newNotificationsCount.then((notificationCount) => {
            resolve(notificationCount);
        }).catch((err) => {
            reject(err);
        });
    });
}

module.exports = {
    getNotificationCountAndData: getNotificationCountAndData,
    getNewNotificationCount: getNewNotificationCount,
}